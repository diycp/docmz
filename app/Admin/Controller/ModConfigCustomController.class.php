<?php

namespace Admin\Controller;

class ModConfigCustomController extends ModController
{
    static $export_menu = array(
        'content' => array(
            '网站设置' => array(
                'index' => array(
                    'title' => '其他设置',
                    'hiddens' => array()
                ),
            )
        )
    );

    public function index()
    {
        $keys = array(
            'docmz_pdf_logo',
        );
        if (IS_POST) {
            foreach ($keys as &$k) {
                tpx_config($k, I('post.' . $k, '', 'trim'));
            }
            foreach (array('docmz_pdf_logo') as $img) {
                $old = tpx_config_get($img);
                $new = upload_tempfile_save_storage('image', $old);
                if ($new) {
                    tpx_config($img, 'data/image/' . $new);
                }
            }
            $this->success('保存成功');
        }

        foreach ($keys as &$k) {
            $this->$k = tpx_config($k);
        }
        $this->docmz_pdf_logo = tpx_config_get('docmz_pdf_logo', 'asserts/res/img/pdf_logo.png');

        $this->display();
    }

}