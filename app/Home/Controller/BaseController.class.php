<?php

namespace Home\Controller;

use Think\Controller;

class BaseController extends Controller
{

    protected function _initialize()
    {
        tpx_upgrade_check();

        if (file_exists($file = './app/Common/Custom/app_env_config.php')) {
            include $file;
        }

        if (!file_exists('./_CFG/install.lock')) {
            header('Location: ' . __ROOT__ . '/admin.php');
            exit();
        }


        // 登陆用户检查
        if (file_exists($file = './app/Home/Controller/Modules/MemberInit.php')) {
            include $file;
        }

        // 未登录可以访问的页面

        $url = strtolower(CONTROLLER_NAME . '/' . ACTION_NAME);
        if (MEMBER_LOGINED) {
            $urlForbiddenWithLogin = array('user/login', 'user/register');
            if (in_array($url, $urlForbiddenWithLogin)) {
                $this->ajaxReturn(array(
                    'status' => 0,
                    'info' => 'login_success',
                    'username' => $this->data_username
                ));
            }
        } else {
            $urlPermitWithoutLogin = array('user/login', 'user/register', 'index/index', 'index/chew');
            if (!in_array($url, $urlPermitWithoutLogin)) {
                $this->ajaxReturn(array(
                    'status' => 0,
                    'info' => 'login_required'
                ));
            }
        }

    }
}