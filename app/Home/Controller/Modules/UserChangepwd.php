<?php
/*
 * 根据邮箱，密码，验证码注册
 * 用于在 /User/login 页面
 *
 * 需要预先设定的值
 *      $title [可选]
 *      $redirect [可选]
 */
if (!defined('THINK_PATH')) {
    exit();
}
if (!MEMBER_LOGINED) {
    $this->redirect('User/login');
}
if (IS_POST) {
    $password = I('post.password', '');
    $password_new = I('post.password-new', '');
    $password_repeat = I('post.password-repeat', '');
    if (!$password) {
        $this->error('旧密码不能为空');
    }
    if (!$password_new) {
        $this->error('新密码不能为空');
    }
    if ($password_new != $password_repeat) {
        $this->error('两次新密码输入不一致');
    }

    $msg = $this->_am->change_password($password, $password_new);
    if (true === $msg) {
        $this->success('修改成功，请重新登录', U('User/logout'));
    } else {
        $this->error($msg);
    }
}
if (empty($title)) {
    $title = tpx_config_get('home_title', '');
}
$this->assign('page_title', '修改密码 - ' . $title);
$this->assign('page_keywords', $title . '修改密码');
$this->assign('page_description', $title . '修改密码');
$this->display();