'use strict';
require.config({
    paths: {
        'app': '../res/app',
        'controller': '../res/controller',
        'router': '../res/router',
        'service': '../res/service'
    }
});

var markdownEditor = null;

require(['angular', 'app', 'jquery.intro',
    'angular-route', 'angular-ui-router', 'angular-ui-tree',
    'jquery.extern', 'jquery.storage', 'jquery.cookie',
    'controller', 'router',
    'service', 'bootstrap', 'lhgdialog.lang', 'lhgdialog.base', 'diff_match_patch'
], function (angular, app, jquery_intro) {

    $(document).keydown(function (event) {
            if ((event.ctrlKey || event.metaKey) && event.which == 83) {
                event.preventDefault();
                try {
                    angular.element(window.parent.$('#command-submit')).scope().submit();
                } catch (e) {
                }
                return false;
            }
        }
    );

    angular.bootstrap($('#ng-app-lazy').get(0), ['app']);

    if ($.cookie('docmz_user_intro')) {
        setTimeout(function () {
            useIntro();
        }, 1000);
    }

    $(document).on('mouseover', '[data-toggle="tooltip"]', function () {
        $(this).tooltip('show');
    });

    $(document).on('click', '.user-intro-btn', function () {
        useIntro();
    });

    var useIntro = function () {
        jquery_intro().setOptions({
            nextLabel: '下一步',
            prevLabel: '上一步',
            skipLabel: '跳过',
            doneLabel: '完成',
            exitOnOverlayClick: false,
            showBullets: false
        }).oncomplete(function () {
            $.removeCookie('docmz_user_intro');
        }).start();
    };

    var resizeFunc = function () {
        $('#b-doc .c-cat').css({
            height: $(window).height() - 40 + 'px'
        });
        $('#b-doc .c-list .list').css({
            height: $(window).height() - 40 - 42 + 'px'
        });
        var o = $('#iframe_editor');
        var w = $('#b-doc .c-body').width();
        if (o.attr('data-fullscreen') == 'true') {
            $('#iframe_editor').css({
                height: $(window).height() + 'px',
                width: w + 320 + 'px'
            });
        } else {
            $('#iframe_editor').css({
                height: $(window).height() - 40 - 41 + 'px',
                width: w + 'px'
            });
        }
        if (markdownEditor) {
            markdownEditor.fullscreen().fullscreen();
        }
    };


    $(window).on('resize', resizeFunc);
    setInterval(resizeFunc, 1000);


});
